<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" type="text/css" href="../styles/menu.css">
      <!-- Bootstrap CSS -->
      <link rel="icon" href="icon.png">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
   </head>
   <body>
      <nav class="navbar navbar-toggleable-md navbar-expand-lg navbar-dark bg-dark ">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
            </button>
         <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <a class="navbar-brand" href="#">Almacigos Santa Clara</a>
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
               <li class="nav-item active">
                  <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#">Flujo de caja</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#">Cuentas</a>
               </li>
               <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Ordenes de compra</a>
                   <div class="dropdown-menu">
                     <a class="dropdown-item"  href="NewOrden.php">Generar</a>
                     <a class="dropdown-item" href="#">Historial</a>
                  </div>
               </li>
               <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Almacigos</a>
                  <div class="dropdown-menu">
                     <a class="dropdown-item" href="#"> En produccion</a>
                     <a class="dropdown-item" href="#"></a>
                     <a class="dropdown-item" href="#">Lista</a>
                     <div class="dropdown-divider"></div>
                     <a class="dropdown-item" href="#" data-toggle="modal" data-target=".almacigos-register-form">Añadir nuevo</a>
                  </div>
               </li>
               <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Clientes</a>
                  <div class="dropdown-menu">
                     <a class="dropdown-item" href="#" data-toggle="modal" data-target=".clients-register-form"  >Añadir nuevo</a>
                  </div>
               </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
               <input class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Search">
               <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
            </form>
         </div>
      </nav>

      <div>
      </div>
      <div class="container">
         <div class="row">
            <!--conexion BD-->
            <?php
               $server = mysql_connect('192.168.10.35',"Felipe", "");
               $db =  mysql_select_db("almacigos_sta_clara",$server);
               $query = mysql_query("select * from bdcliente");
               ?>
            <table  id="display"  class="display  table-striped table-inverse table table-responsive">
               <thead id="tblHead">
                  <tr class=" success">
                     <th>Nombre</th>
                     <th >Empresa</th>
                     <th>Telefono</th>
                     <th>Celular</th>
                     <th>Correo</th>
                     <th>Direccion</th>
                     <th class="text-right">Estado</th>
                  </tr>
               </thead>
               <tbody>
                  <?php 
                     // 0 is the index of the field, not the row
                     $imgAct = '../images/icons/activo.png';
                     $imgIna = '../images/icons/inactivo.png';
                      while($row = mysql_fetch_array($query)):;?>
                  <tr>
                     <td><?php echo $row[1]?></td>
                     <td><?php echo $row[2]?></td>
                     <td><?php echo $row[3]?></td>
                     <td><?php echo $row[4]?></td>
                     <td ><a href="mailto:'<?php echo $row[5]?>'"><?php echo $row[5]?></a></td>
                     <td><?php echo $row[6]?></td>
                     <?php $Estado = $row[7]; ?>
                     <?php if($Estado > 0): ?>
                     <td ><?php echo '<img src="'.$imgAct.'" width="50px">'?></td>
                     <?php else: ?>
                     <td><?php echo '<img src="'.$imgIna.'" width="50px">'?></td>
                     <?php endif; ?>
                  </tr>
                  <?php endwhile;?>
               </tbody>
            </table>
         </div>
      </div>
      <div id="inferior" class="flotante button">Nuevo<span glyphicon glyphicon-leaf></span></div>
      <!--  Register Client Modal-->
      <div class="modal fade clients-register-form" role="dialog">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">
                  <span class="glyphicon glyphicon-remove"></span>
                  </button>
                  <ul class="nav nav-tabs">
                     <li><a data-toggle="tab" href="#registration-form" > Registrar </a></li>
                     <li><a>||</a></li>
                     <li><a data-toggle="tab" href="..."> Modificar <span class="glyphicon glyphicon-pencil"></span></a></li>
                  </ul>
               </div>
               <div class="modal-body">
                  <div class="tab-content">
                     <div id="login-form" class="tab-pane fade in active">
                     </div>
                     <div id="registration-form" class="tab-pane fade">
                        <form  action="../envio_cliente.php" method="POST">
                           <div class="form-group">
                              <label for="name">Nombre:</label>
                              <input type="text" class="form-control" id="name" placeholder="Nombre del cliente" name="name" required>
                           </div>
                           <div class="form-group">
                              <label for="empr">Empresa:</label>
                              <input type="text" class="form-control" id="Emp" placeholder="Empresa" name="Emp">
                           </div>
                           <div class="form-group">
                              <label for="newTel">Telefono:</label>
                              <input type="tel"  class="form-control" id="newTel" placeholder="Telefono" name="newTel"  pattern="[0-9]{8}">
                           </div>
                           <div class="form-group">
                              <label for="newCel">Celular:</label>
                              <input type="tel"  class="form-control" id="newCel" placeholder="Celular" name="newCel"  pattern="[0-9]{8}">
                           </div>
                           <div class="form-group">
                              <label for="newemail">Correo:</label>
                              <input type="email" class="form-control" id="newemail" placeholder="Correo" name="newemail">
                           </div>
                           <div class="form-group">
                              <label for="newdir">Direccion:</label>
                              <textarea class="form-control" rows="5"  id="newDir" placeholder="Direccion" name="newDir" required></textarea>
                           </div>
                           <button type="submit" class="btn btn-success">Guardar</button>
                        </form>
                     </div>
                  </div>
               </div>
               <!--                                    <div class="modal-footer">-->
               <!--                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
               <!--                                    </div>-->
            </div>
         </div>
      </div>
      <!--  Register almacigo Modal-->
      <div class="modal fade almacigos-register-form" role="dialog">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">
                  <span class="glyphicon glyphicon-remove"></span>
                  </button>
                  <ul class="nav nav-tabs">
                     <li><a data-toggle="tab" href="#registrationAlm-form" > Nuevo </a></li>
                     <li><a>||</a></li>
                     <li><a data-toggle="tab" href="..."> Modificar <span class="glyphicon glyphicon-pencil"></span></a></li>
                  </ul>
               </div>
               <div class="modal-body">
                  <div class="tab-content">
                     <div id="login-form" class="tab-pane fade in active">
                     </div>
                     <div id="registrationAlm-form" class="tab-pane fade">
                        <form  action="../envio_almacigo.php" method="POST">
                           <div class="form-group">
                              <label for="name">Cultivo:</label>
                              <input type="text" class="form-control" id="cult" placeholder="Tipo de cultivo" name="cult" required>
                           </div>
                           <div class="form-group">
                              <label for="empr">Variedad:</label>
                              <input type="text" class="form-control" id="var" placeholder="Variedad" name="var" required>
                           </div>
                           <div class="form-group">
                              <label for="newGer">Tiempo de germinacion:</label>
                              <input type="integrity"  class="form-control" id="germ" placeholder="Cantidad de días" name="germ"  required>
                           </div>
                           <div class="form-group">
                              <label for="newPro">Tiempo en produccion:</label>
                              <input type="integrity"  class="form-control" id="prod" placeholder="Cantidad de días en Invernadero" name="prod" required>
                           </div>
                            <div class="form-group">
                              <label for="newPro">Proveedor:</label>
                              <input type="integrity"  class="form-control" id="prov" placeholder="Cantidad de días en Invernadero" name="prov" required>
                           </div>
                           <button type="submit" class="btn btn-success">Guardar</button>
                        </form>
                     </div>
                  </div>
               </div>
               <!--                                    <div class="modal-footer">-->
               <!--                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
               <!--                                    </div>-->
            </div>
         </div>
      </div>
      <!--  Client Modal-->
      <div class="modal fade" id="myModal">
         <div class="modal-dialog  modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               </div>
               <div class="modal-body">
                  <div class="panel-heading">
                     <h3 class="panel-title">Clientes</h3>
                  </div>
                  <!----------Tablaa---------->
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
               </div>
            </div>
            <!-- /.modal-content -->
         </div>
         <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
       <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
       <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
      <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="../scripts/menu.js"></script>
      <script type="text/javascript" src="../scripts/jquery.js"></script>
      <script type="text/javascript" src="../table/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="../table/js/dataTables.bootstrap.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
   </body>
</html>