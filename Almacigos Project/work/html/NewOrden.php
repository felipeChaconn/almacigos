<!DOCTYPE html>
<html lang="en">
  <head>
     <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" type="text/css" href="../styles/new_orden.css">
      
      <!-- Bootstrap CSS -->
      <link rel="icon" href="icon.png">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
          <link rel="stylesheet" type="text/css" href="../styles/datepicker.css">
   </head>
   <body>
    <div class="container cont">
     <div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2 main">
      <form action="#" method="post" class="form-horizontal">
        <legend><b style="color:#fff">Orden de compra:</b></legend>
        <div class="form-group">
          <label class="col-md-3 control-label">Cliente:</label>
          <div class="col-md-9">
            <div select-style2 >
              <!--CONEXION A BASE DE DATOS -->
              <?php
                 $con=mysql_connect('192.168.10.35','Felipe','') or die(mysql_error());  
                  mysql_select_db('almacigos_sta_clara') or die("cannot select DB");  
              ?>
            <select size="1" autocomplete="off" id="selectbox"class="custom-select">
                <option selected>Seleccione el nombre:</option>
             <?php
              $query="select * from bdcliente";
                      $res=mysql_query($query);
              while ($row=mysql_fetch_array($res)){
              ?>
              <option value="<?php echo $row['Cliente']?>"> <?php echo $row['Cliente']; ?> </option>
              <?php }  ?>
             </select>
             </div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Almacigo:</label>
          <div class="col-md-9">
            <select size="1" autocomplete="off" id="selectbox"class="custom-select">
                <option selected>Seleccione la variedad del cultivo:</option>
             <?php
              $query="select * from bdalmacigo";
                      $res=mysql_query($query);
              while ($row=mysql_fetch_array($res)){
              ?>
              <option value="<?php echo $row['Variedad']?>"> <?php echo $row['Variedad']; ?> </option>
              <?php }  ?>
             </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Cantidad:</label>
          <div class="col-md-9">
            <input   type="number" min="1" placeholder="Cantidad de plantas" class="form-control" required>
          </div>  
        </div>
        <div class="form-group">
          <label class="col-md control-label">Fecha aproximada de siembra:</label>
          <div class="col-md-9">
            <input type="text" id="datepicker2" name="datepicker2" class="date" >
          </div>
        </div>
        <div class="form-group">
          <label class="col-md control-label">Fecha aproximada de retiro:</label>
          <div class="col-md-9">
             <input type="text" id="datepicker1" name="datepicker1" class="date" >
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Address:</label>
          <div class="col-md-9">
            <textarea  class="form-control" width="100%"></textarea>
          </div>
        </div>
         <div class="form-group" >      
           <label class="col-md control-label">Cant semilla por Celda:</label>
          <div class="col-md-9">
        <label class="checkbox-inline">
              <input type="checkbox">Sencillas
        </label>
         <label class="checkbox-inline">
              <input type="checkbox">Dobles
            </label>
         </div>
         </div>
        <div class="form-group" >
           <label class="col-md control-label">Bandeja:</label>
        <div class="col-md-9">
        <label class="radio-inline">
              <input type="radio">92
        </label>
        <label class="radio-inline">
              <input type="radio">48
        </label>
        <label class="radio-inline">
              <input type="radio">65
        </label>
        </div>
        </div>
        <div class="form-group">
          <div class="col-md-5 col-md-offset-7"> 
            <button class="btn btn-warning pull-right">
              <span class="glyphicon glyphicon-repeat">Reset</span>
            </button>
            <button class="btn btn-primary pull-right save">
              <span class="glyphicon glyphicon-floppy-disk ">Save</span>
            </button>
          </div>  
        </div>
      </form>
    </div>
    </div>
    <script type="text/javascript" src="../scripts/jq.js"></script>
    <script type="text/javascript" src="../scripts/bootstrap-datepicker.js"></script>
     <script >
      $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
    
       
          
   </body>
   </html>