<!--
 Copyright 2016 Google Inc.
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="styles/login.css">
    <!-- Bootstrap CSS -->
    <link rel="icon" href="icon.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
  <div class="container">
    <div class="row">

     <div class="col-md-12">
        <div class="wrap">
           <p class="form-title">
            Almacigos Santa Clara</p>

            <form class= "login" action="" method="POST">
                <input type="text" name="user" placeholder="Username" />
                  <div class="password">
              <input type="password" name="pass" id="passwordfield" placeholder="password">
                        <span class="glyphicon glyphicon-eye-open"></span>
                         </div>
                <input type="submit" value="Login" name="submit" class="btn btn-success btn-sm" />
            </form>
<?php 
            if(isset($_POST["submit"])){  
            
            if(!empty($_POST['user']) && !empty($_POST['pass'])) {  
            $user=$_POST['user'];  
            $pass=$_POST['pass'];  
            
            $con=mysql_connect('192.168.10.35','Felipe','') or die(mysql_error());  
            mysql_select_db('almacigos_sta_clara') or die("cannot select DB");  
            
            $query=mysql_query("SELECT * FROM users WHERE Nombre='".$user."' AND Passwords ='".$pass."'");  
            $numrows=mysql_num_rows($query);  
            if($numrows!=0)  
            {  
            while($row=mysql_fetch_assoc($query))  
            {  
            $dbusername=$row['Nombre'];  
            $dbpassword=$row['Passwords'];  
        }  
        
        if($user == $dbusername && $pass == $dbpassword)  {  
        session_start();  
        $_SESSION['sess_user']=$user;
        header("Location:html/Menu.php");
    }
} else {  
 $message = "Username and/or Password incorrect.\\nTry again.";
  echo "<script type='text/javascript'>alert('$message');</script>";
 header(" Location: index.html");
}  

} else {  
echo "All fields are required!";  
}  
}  
?>  
<div class="remember-forgot">
    <div class="row">
        
         </div>
</div>

</div>
</div>
</div>
<div class="posted-by">Desing By: <a>Felipe Chacon</a></div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script type="text/javascript" src="scripts/login.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</body>
</html>